package com.automation.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {

    @FindBy(linkText = "Sign in")
    private WebElement signInButton;

    public void clickOnSignIn(){
        signInButton.click();
    }
}
