package com.automation.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class BrowserLaunch {


    @Test
    public void openChromeAndNavigateToEbay() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.google.com");
         WebElement r = driver.findElement(By.name("q"));
        r.sendKeys("Fair trade audio");
        // press ENTER
        r.sendKeys(Keys.RETURN);
        Thread.sleep(2000);
        driver.findElement(By.xpath("//span[@class='j0joJb']")).click();
        Thread.sleep(15000);
        driver.findElement(By.xpath("//button[@aria-label='Play']")).click();
        Thread.sleep(2000);
       // driver.findElement(By.xpath("//div[@aria-label='Seek slider']")).click();

        //driver.executeScript()

        //driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).sendKeys("Fair trade audio");

        //driver.findElement(By.xpath("//input[@data-ved='0ahUKEwjUqs7biJT2AhVJj4kEHTD9BnoQ4dUDCA0']")).click();
        //Thread.sleep(5000);
        //driver.findElement(By.xpath("//yt-formatted-string[@aria-label='Fair Trade by Drake 4 minutes, 52 seconds 1,051,915 views']")).click();

       /* Thread.sleep(5000);
        driver.quit();*/
    }
    @Test
    public void openFireFox () throws InterruptedException {
    System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
            FirefoxDriver driver = new FirefoxDriver();
            driver.get("https://www.ebay.com");

        Thread.sleep(5000);
        driver.quit();

    }
}
