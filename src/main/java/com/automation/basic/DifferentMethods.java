package com.automation.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;

public class DifferentMethods extends CommonApi {


    @Test
    public void waitExample() {
        //explicit wait
        WebElement element = driver.findElement(By.id("gh-ac"));

        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(10));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }


    @Test
    public void dropDownTest() {
        launchBrowser("chrome", "https://www.ebay.com");

        driver.findElement(By.id("gh-ac")).sendKeys("Java Books");

        //getting all values from a dropdown
        List<WebElement> categories = driver.findElements(By.xpath("//select[@id='gh-cat']/option"));

        for (int i = 0; i < categories.size(); i++) {
            System.out.println(categories.get(i).getText());
        }

        WebElement categorySelector = driver.findElement(By.xpath("//select[@id='gh-cat']"));

        Select select = new Select(categorySelector);
        select.selectByIndex(4);
        waitingPeriod(5);

        select.selectByVisibleText("Art");
        waitingPeriod(5);

        quitBroweser();
    }


    @Test
    public void mouseHoverTest() {
        launchBrowser("chrome", "https://www.ebay.com");

        WebElement motors = driver.findElement(By.linkText("Motors"));

        Actions actions = new Actions(driver);
        actions.moveToElement(motors).build().perform();

        waitingPeriod(5);

        driver.findElement(By.linkText("Cars & Trucks")).click();

        waitingPeriod(5);

        quitBroweser();
    }


    @Test
    public void scrollDownToNewsTest() {
        launchBrowser("chrome", "https://www.ebay.com");

        WebElement news = driver.findElement(By.linkText("News"));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", news);
        waitingPeriod(5);

        quitBroweser();
    }

    @Test
    public void scrollDownToFacebookTest() {
        launchBrowser("chrome", "https://www.ebay.com");

        WebElement facebook = driver.findElement(By.linkText("Facebook"));

        scrollDownTo(facebook);
        waitingPeriod(5);

        quitBroweser();
    }

    @Test
    public void scrollDownTest() {
        launchBrowser("chrome", "https://www.ebay.com");

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
        waitingPeriod(5);

        js.executeScript("window.scrollBy(0,1000)");
        waitingPeriod(5);

        quitBroweser();
    }

    @Test
    public void dragNDropTest() {
        launchBrowser("chrome", "https://demo.guru99.com/test/drag_drop.html");
        WebElement from = driver.findElement(By.xpath("(//a[@class='button button-orange'])[2]"));
        WebElement to = driver.findElement(By.id("amt7"));

        Actions actions = new Actions(driver);

        actions.dragAndDrop(from, to).build().perform();

        waitingPeriod(5);

        quitBroweser();
    }


    @Test
    public void alertTest() {
        launchBrowser("chrome", "https://demo.guru99.com/test/delete_customer.php");
        driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys("1");
        driver.findElement(By.xpath("//input[@name='submit']")).click();

        waitingPeriod(2);

        driver.switchTo().alert().accept();

        String alertData = driver.switchTo().alert().getText();

        System.out.println(alertData);

        //driver.switchTo().alert().sendKeys("");

        waitingPeriod(5);

        quitBroweser();
    }

    @Test
    public void clicksTest() {
        Actions actions = new Actions(driver);
        WebElement element = driver.findElement(By.xpath(""));
        actions.doubleClick(element).build().perform();

        actions.contextClick(element).build().perform();
    }


    @Test
    public void iFrameTest() {
        launchBrowser("chrome", "https://demoqa.com/frames");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");

        waitingPeriod(5);

        driver.switchTo().frame("frame2");
        js.executeScript("window.scrollBy(0,1000)");


        driver.switchTo().defaultContent();
        waitingPeriod(5);
        quitBroweser();
    }


    @Test
    public void radioButton() {
        launchBrowser("chrome", "https://www.aa.com/");
        waitingPeriod(2);

        // from the page click on the one way if that's not selected
        List<WebElement> data = driver.findElements(By.xpath("//input[@type='radio']"));

        for (WebElement datum : data) {
            String value = datum.getAttribute("value");
            if (value.equalsIgnoreCase("oneway")) {

                System.out.println(datum.isEnabled());

                System.out.println(datum.isSelected());
                driver.findElement(By.xpath("//label[@for='flightSearchForm.tripType.oneWay']")).click();
                System.out.println(datum.isSelected());
                break;
            }
        }


        waitingPeriod(5);
        quitBroweser();
    }


}
