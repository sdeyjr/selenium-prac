package com.automation.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class FairTrade extends CommonApi {
    @Test
    public void openYoutubeAndPlayFairTrade() throws InterruptedException {
        CommonApi commonApi = new CommonApi();
        commonApi.launchBrowser("Chrome", "https://www.google.com");
        WebElement r = commonApi.driver.findElement(By.name("q"));
        r.sendKeys("Fair trade audio");
        // press ENTER
        r.sendKeys(Keys.RETURN);
        Thread.sleep(2000);
        commonApi.driver.findElement(By.xpath("//span[@class='j0joJb']")).click();
        Thread.sleep(15000);
        commonApi.driver.findElement(By.xpath("//button[@aria-label='Play']")).click();
        Thread.sleep(2000);
        // driver.findElement(By.xpath("//div[@aria-label='Seek slider']")).click();
        //driver.executeScript()

        //driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).sendKeys("Fair trade audio");

        //driver.findElement(By.xpath("//input[@data-ved='0ahUKEwjUqs7biJT2AhVJj4kEHTD9BnoQ4dUDCA0']")).click();
        //Thread.sleep(5000);
        //driver.findElement(By.xpath("//yt-formatted-string[@aria-label='Fair Trade by Drake 4 minutes, 52 seconds 1,051,915 views']")).click();

        commonApi.waitingPeriod(5);
        commonApi.quitBroweser();
    }

    @Test
    public void justTesting () {
        launchBrowser("chrome", "https://www.google.com");
    }
}
