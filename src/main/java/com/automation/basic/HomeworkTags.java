package com.automation.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

public class HomeworkTags extends CommonApi {
    @Test
    public void openStackAndGetTags() {

        launchBrowser("Chrome", "https://stackoverflow.com/tags");
        List<WebElement> postTags = driver.findElements(By.xpath("//a[@class='post-tag']"));
        System.out.println("These are the tags :");
        for (int i = 0; i < postTags.size(); i++) {
            WebElement postMalone = postTags.get(i);
            System.out.println(i + " " + postMalone.getText());
        }

        waitingPeriod(2);
        quitBroweser();
    }
}
